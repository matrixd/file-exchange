#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

import webapp2
import jinja2
import os
import socket
import string
import db
import time
import sys
jinja_environment = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))
class Srv():
    class HelloWebapp2(webapp2.RequestHandler):
	    def get(self):
		    template = jinja_environment.get_template('index.html')
		    self.response.write(template.render({'public':False}))
	    def post(self):
		    self.redirect('/get/%s' % self.request.get('code'))
    class GetFiles(webapp2.RequestHandler):
        def getUser(self):
	        user = {}
	        user['ip'] = self.request.remote_addr
	        user['browser'] = self.request.headers['User-Agent']
	        user['host'] = socket.gethostbyaddr(user['ip'])[0]
	        print 'user connected '+user['ip']+" "+user['host']
	        return user
	    def get(self, key):
		    d = db.DB()
		    f = d.getByKey(key, self.getUser())
		    if f:
			    print f['path']
			    of = open(f['path'])
			    print of
			    fname = f['path'].split('/')[-1]
			    statinfo = os.stat(f['path'])
			    self.response.headers['Content-Type'] = 'application/octet-stream'
			    self.response.headers['Accept-Ranges'] = 'bytes'
			    self.response.headers['Content-Length'] = statinfo.st_size
			    self.response.headers['Content-Disposition'] = 'attachment; filename="'+fname+'"'
			    self.response.write(of.read())
			    print 'download started by client'
		    else:
			    template = jinja_environment.get_template('index.html')
			    self.response.write(template.render({'content':'<b>No Such File</b>', 'public':''}))
    class Public(webapp2.RequestHandler):
	    def get(self):
		    if True:
			    d = db.DB()
			    rows = d.getPublic()
			    if rows:
				    content = jinja_environment.get_template('public.html')
				    template = jinja_environment.get_template('index.html')
				    self.response.write(template.render({'content':content.render({'rows':rows})}))
			    else:
				    template = jinja_environment.get_template('index.html')
				    self.response.write(template.render({'content':'<b>No Public Files sorry :(</b>', 'public':False}))
		    else:
			    template = jinja_environment.get_template('index.html')
			    self.response.write(template.render({'content':'<b>No files with public access</b>'}))

    def __init__(self, settings):
        self.settings = settings
        self.app = webapp2.WSGIApplication([
	        ('/', self.HelloWebapp2),
	        ('/get/(\w+)', self.GetFiles),
	        ('/public', self.Public),
	        ], debug=True)
	    from paste import httpserver
	    httpserver.serve(self.app, host='0.0.0.0', port=settings['port'])
