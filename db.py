#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#Class to manage with table

import sqlite3
import hashlib

class DB:
	def parseRow(self, row):
		#colums: id,fname,shortc,path,public,last,count
		obj = {}
		obj['id'] = row[0]
		obj['fname'] = row[1]
		obj['shortc'] = row[2]
		obj['path'] = row[3]
		obj['public'] = row[4]		
		obj['last'] = row[5]
		obj['count'] = row[6]
		return obj
		
	def parseDownload(self, row):
		#columns: id, fileid, ip, hostname, browser, time
		obj = {}
		obj['id'] = row[0]
		obj['fileid'] = row[1]
		obj['ip'] = row[2]
		obj['hostname'] = row[3]
		obj['browser'] = row[4]
		obj['time'] = row[5]
		return obj
		
	def connect(self):
		conn = sqlite3.connect('main.db')
		return conn

    def getAllSettings(self):
        try:
            conn = self.connect()
			cur = conn.cursor()
			cur.execute("SELECT * FROM settings;")
            obj = {}
            for row in cur.fetchall():
                obj[row[0]] = row[1]
            cur.close()
			conn.close()
            return obj
        except:
            return False
	def getByKey(self, c, client):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("SELECT * FROM files WHERE shortc = '"+c+"';")
			obj = self.parseRow(cur.fetchone())
			cur.close()
			cur = conn.cursor()
			cur.execute("UPDATE files SET last = CURRENT_TIMESTAMP, count = count+1 WHERE id = '%d';" % obj['id'])
			conn.commit()
			cur.close()
			cur = conn.cursor()
			cur.execute("INSERT INTO downloads (fileid, ip, hostname, browser, time) VALUES('%d', '%s', '%s', '%s', CURRENT_TIMESTAMP);" % (obj['id'], client['ip'], client['host'], client['browser']))
			conn.commit()
			cur.close()
			conn.close()
			return obj
		except:
			return False
	def getAll(self):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("SELECT * FROM files;")
			rows = cur.fetchall()
			lst = []
			for row in rows:
				lst.append(self.parseRow(row))
			cur.close()
			conn.close()
			return lst
		except:
			return False

	def getClients(self, n):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("SELECT * FROM downloads WHERE fileid='%d';" % n)
			rows = cur.fetchall()
			lst = []
			for row in rows:
				lst.append(self.parseDownload(row))
			cur.close()
			conn.close()
			return lst
		except:
			return False
			
	#deleting by id
	def delFile(self, n):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("DELETE FROM files WHERE id='%d';" % n)
			conn.commit()
			cur.close()
			cur = conn.cursor()
			cur.execute("DELETE FROM downloads WHERE file='%d';" % n)
			conn.commit()
			cur.close()
			conn.close()
			return "file with id %d was deleted" % n
		except:
			return False
	#deletes all files from list + all clients info
	def delAll(self):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("DELETE FROM files;")
			conn.commit()
			cur.close()
			cur = conn.cursor()
			cur.execute("DELETE FROM downloads;")
			conn.commit()
			cur.close()
			conn.close()
			return True
		except:
			return False
	#adding a file into db, returning shortcode
	def add(self, fname, path, public):
		try:
			conn = self.connect()
			cur = conn.cursor()
			m = hashlib.md5()
			m.update(path.encode('utf-8'))
			c = m.hexdigest()[:10]
			cur.execute("INSERT INTO files (fname, path, shortc, public) VALUES('%s', '%s', '%s', '%d')" % (fname, path, c, public))
			conn.commit()
			cur.close()
			conn.close()
			return c
		except sqlite3.OperationalError as e:
			print e
			return False
	#changing all settings
	def saveSettigns(self, settings):
	    try:
	        conn = self.connect()
			cur = conn.cursor()
			query = "delete from settings;\n"
			for key,val in settings.iteritems():
			    query += "insert into settings (name, val) values('%s', '%s');\n" % (key,val)
			cur.executescript(query)
			conn.commit()
			cur.close()
			conn.close()
	    except sqlite3.OperationalError as e:
			print e
			return False
	#getting all public files
	def getPublic(self):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute('SELECT * FROM files WHERE public=1')
			rows = cur.fetchall()
			lst = []
			for row in rows:
				lst.append(self.parseRow(row))
			cur.close()
			conn.close()
			return lst
		except:
			return False
	def install(self):
		try:
			conn = self.connect()
			cur = conn.cursor()
			cur.execute("""
			CREATE TABLE "files" (
				"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
				"fname" TEXT NOT NULL,
				"shortc" TEXT NOT NULL,
				"path" TEXT NOT NULL,
				"public" INTEGER  NOT NULL  DEFAULT (0),
				"last" TEXT,
				"count" INTEGER  DEFAULT (0)
			);
			""")
			cur.execute("""
			CREATE TABLE "settings" (
				"name" TEXT PRIMARY KEY NOT NULL,
				"val" TEXT NOT NULL
			);
			""")
			cur.execute("""
			CREATE TABLE "downloads" (
				"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
				"fileid" INTEGER NOT NULL,
				"ip" TEXT NOT NULL,
				"hostname" TEXT NOT NULL,
				"browser" TEXT NOT NULL,
				"time" TEXT
			);
			""")
			#inserting default settings into db"
			cur.executescript("""
                insert into "settings" (name,val) values("port", "8080");\n
                insert into "settings" (name,val) values("ip", "127.0.0.1");\n
                insert into "settings" (name,val) values("public", "0");\n
            """)
            conn.commit()
			cur.close()
			conn.close()
			return "Installed"
		except:
			return "Smth wrong"
