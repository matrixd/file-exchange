#!/usr/bin/python
# -*- coding: utf-8 -*- 
import sys
from PyQt4 import QtCore, QtGui
from mainwindow import *
from linkwindow import *
from info import *
from pereferences import *
import server
import threading
import db as database
import codecs
import os
import urllib2

class StartQT4(QtGui.QMainWindow):
	def __init__(self, settings, parent=None):
		QtGui.QWidget.__init__(self, parent)
		self.settings = settings
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.showAllFiles()
		self.showIp()
		QtCore.QObject.connect(self, QtCore.SIGNAL("showIp()"), self.showIp)
		QtCore.QObject.connect(self.ui.addButton, QtCore.SIGNAL("clicked()"), self.addFile)
		QtCore.QObject.connect(self.ui.delButton, QtCore.SIGNAL("clicked()"), self.delFile)
		QtCore.QObject.connect(self.ui.actionPereferences, QtCore.SIGNAL("triggered()"), self.showSettings)
		QtCore.QObject.connect(self.ui.tableWidget, QtCore.SIGNAL("cellDoubleClicked(int ,int)"), self.showLink)
		QtCore.QObject.connect(self.ui.infoButton, QtCore.SIGNAL("clicked()"), self.showInfo)
		QtCore.QObject.connect(self.ui.refreshButton, QtCore.SIGNAL("clicked()"), self.showAllFiles)
		QtCore.QObject.connect(self.ui.ipRefreshButton, QtCore.SIGNAL("clicked()"), self.refreshIp)
		
	def addRow(self, widget, itemlst):
		rows = widget.rowCount()+1
		widget.setRowCount(rows)
		i=0
		for s in itemlst:
			if s:
				item = QtGui.QTableWidgetItem(s)
				widget.setItem(rows-1,i,item)
			i=i+1
	
	def showAllFiles(self):
		db = database.DB()
		self.ui.tableWidget.clearContents()
		self.ui.tableWidget.setRowCount(0)
		res = db.getAll()
		if res:
			for item in res:
				lst = []
				#colums: id,fname,shortc,path,public,last,count
				lst.append(str(item['id']))
				lst.append(item['fname'])
				lst.append(item['shortc'])
				lst.append(item['path'])
				lst.append(str(bool(item['public'])))
				lst.append(item['last'])
				lst.append(item['count'])
				lst.append(self.addRow(self.ui.tableWidget, lst))
			
	def addFile(self):
		fd = QtGui.QFileDialog(self)
		shortcLst = []
		for tmp in fd.getOpenFileNames():
			path = str(tmp.toUtf8()).decode('utf8')
			print path
			if os.path.isfile(path):
				db = database.DB()
				fname = path.split('/')[-1]
				print fname
				c = db.add(fname, path, 0)
				shortcLst.append(c)
		self.showAllFiles()
		d = LinkDialog()
		d.showLink(shortcLst[0])
		
	def delFile(self):
		db = database.DB()
		for item in self.ui.tableWidget.selectedItems():
			if item.column() == 0:
				 db.delFile(int(item.text()))
		self.showAllFiles()

	def showInfo(self):
		n = int(self.ui.tableWidget.selectedItems()[0].text())
		db = database.DB()
		clients = db.getClients(n)
		if len(clients)>0:
			InfoDialog(db.getClients(n), self.ui.tableWidget.selectedItems()[1].text())
		else:
			alertMessage("Nobody has downloaded this file yet")
	def showLink(self, row, col):
		d = LinkDialog()
		d.showLink(self.ui.tableWidget.item(row, 2).text(), self.settings['ip'], self.settings['port'])
	def showSettings(self):
		s = SettingsDialog(self.settings)
	def refreshIp(self):
		self.ui.ipRefreshButton.enabled = False
		self.ui.ipLabel.setText('<i>Refreshing...</i>')
		def f(self):
			u = urllib2.urlopen("http://ifconfig.me/ip")
			self.settings['ip'] = u.readline()[:-1]
			self.emit(QtCore.SIGNAL('showIp()'))
			db = database.DB()
			db.saveSettigns(self.settings)
			
		#thread for ip resfresh
		threading.Thread(target=f, kwargs={'self': self}).start()
		
	def showIp(self):
		self.ui.ipRefreshButton.enabled = True
		self.ui.ipLabel.setText('your current ip is: %s' % self.settings['ip'])
		self.ui.portLabel.setText('port: %s' % self.settings['port'])

class LinkDialog(QtGui.QDialog):
	def __init__(self, parent=None):
		QtGui.QDialog.__init__(self,parent)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		QtCore.QObject.connect(self.ui.closeButton, QtCore.SIGNAL("clicked()"), self.close)
	def showLink(self, shortcode, ip, port):
		line = ''
		if port == '80':
			line = 'http://%s/get/%s' % (ip, shortcode)
		else:
			line = 'http://%s:%s/get/%s' % (ip, port, shortcode)
		self.ui.lineEdit.setText(line)
		self.ui.lineEdit.selectAll()
		self.exec_()

def alertMessage(message):
	mb = QtGui.QMessageBox()
	mb.setText(message)
	mb.exec_()

class InfoDialog(QtGui.QDialog):
	def __init__(self, downloads, fname, parent=None):
		QtGui.QDialog.__init__(self,parent)
		self.ui = Ui_InfoDialog()
		self.ui.setupUi(self)
		self.downloads = downloads
		QtCore.QObject.connect(self.ui.tableWidget, QtCore.SIGNAL("cellClicked(int ,int)"), self.showMore)
		self.ui.fname.setText('<b>filename:</b> %s' % fname)
		self.refresh()
		self.ui.tableWidget.setCurrentCell(0,0)
		self.showMore(0,0)
		self.exec_()
	def showMore(self, row, col):
		dm = self.downloads[row]
		self.ui.label.setText('<b>time:</b> %s<br><b>ip:</b> %s<br><b>hostname:</b> %s<br><b>browser:</b> %s<br>' % (dm['time'], dm['ip'], dm['hostname'], dm['browser']))
	def refresh(self):
		self.ui.tableWidget.clearContents()
		self.ui.tableWidget.setColumnWidth(0, 200)
		self.ui.tableWidget.setColumnWidth(2, 250)
		self.ui.tableWidget.setColumnWidth(1, 250)
		rows = 0
		for dm in self.downloads:
			rows = rows+1
			self.ui.tableWidget.setRowCount(rows)
			item = QtGui.QTableWidgetItem(dm['ip'])
			self.ui.tableWidget.setItem(rows-1,0,item)
			item = QtGui.QTableWidgetItem(dm['time'])
			self.ui.tableWidget.setItem(rows-1,2,item)
			item = QtGui.QTableWidgetItem(dm['hostname'])
			self.ui.tableWidget.setItem(rows-1,1,item)

class SettingsDialog(QtGui.QDialog):
	def __init__(self, settings, parent=None):
		QtGui.QDialog.__init__(self,parent)
		self.ui = Ui_PereferencesDialog()
		self.ui.setupUi(self)
		self.settings = settings
		self.ui.ipEdit.setText(self.settings['ip'])
		self.ui.portEdit.setText(self.settings['port'])
		self.ui.publicCombo.setCurrentIndex(int(self.settings['public']))
		QtCore.QObject.connect(self.ui.saveButton, QtCore.SIGNAL("clicked()"), self.save)
		self.exec_()
	def save(self):
		if self.ui.publicCombo.currentIndex() != self.settings['public']:
			self.settings['public'] = self.ui.publicCombo.currentIndex()
		if self.ui.portEdit.text() != self.settings['port']:
			try:
				self.settings['port'] = int(self.ui.portEdit.text())
				alertMessage('relaunch programm in order to apply settings')
			except:
				alertMessage('port must be a number')
		if self.ui.ipEdit.text() != self.settings['ip']:
			self.parent.settings['ip'] = self.ui.ipEdit.text()
			alertMessage('relaunch programm in order to apply settings')
		db = database.DB()
		db.saveSettigns(self.settings)
		
#starting the app
if os.path.isfile('main.db') == False:
	db = database.DB()
	db.install()
def launch(settings):
	app = QtGui.QApplication(sys.argv)
	myapp = StartQT4(settings)
	myapp.show()
	sys.exit(app.exec_())

db = database.DB()
sets = db.getAllSettings()
gui = threading.Thread(target=launch, kwargs={'settings': sets})
gui.start()
serv = threading.Thread(target=server.Srv, kwargs={'settings': sets})
serv.start()
gui.join()
os._exit(1)
