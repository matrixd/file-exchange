# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pereferences.ui'
#
# Created: Thu Apr 25 01:10:32 2013
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_PereferencesDialog(object):
    def setupUi(self, PereferencesDialog):
        PereferencesDialog.setObjectName(_fromUtf8("PereferencesDialog"))
        PereferencesDialog.resize(249, 175)
        self.saveButton = QtGui.QPushButton(PereferencesDialog)
        self.saveButton.setGeometry(QtCore.QRect(120, 140, 98, 27))
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.portEdit = QtGui.QLineEdit(PereferencesDialog)
        self.portEdit.setGeometry(QtCore.QRect(100, 50, 113, 21))
        self.portEdit.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.portEdit.setObjectName(_fromUtf8("portEdit"))
        self.label = QtGui.QLabel(PereferencesDialog)
        self.label.setGeometry(QtCore.QRect(20, 50, 57, 15))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(PereferencesDialog)
        self.label_2.setGeometry(QtCore.QRect(10, 90, 121, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.publicCombo = QtGui.QComboBox(PereferencesDialog)
        self.publicCombo.setGeometry(QtCore.QRect(120, 90, 81, 22))
        self.publicCombo.setObjectName(_fromUtf8("publicCombo"))
        self.publicCombo.addItem(_fromUtf8(""))
        self.publicCombo.addItem(_fromUtf8(""))
        self.label_3 = QtGui.QLabel(PereferencesDialog)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 64, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.ipEdit = QtGui.QLineEdit(PereferencesDialog)
        self.ipEdit.setGeometry(QtCore.QRect(100, 10, 113, 27))
        self.ipEdit.setObjectName(_fromUtf8("ipEdit"))

        self.retranslateUi(PereferencesDialog)
        QtCore.QMetaObject.connectSlotsByName(PereferencesDialog)

    def retranslateUi(self, PereferencesDialog):
        PereferencesDialog.setWindowTitle(QtGui.QApplication.translate("PereferencesDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.saveButton.setText(QtGui.QApplication.translate("PereferencesDialog", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("PereferencesDialog", "port:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("PereferencesDialog", "Public files", None, QtGui.QApplication.UnicodeUTF8))
        self.publicCombo.setItemText(0, QtGui.QApplication.translate("PereferencesDialog", "disabled", None, QtGui.QApplication.UnicodeUTF8))
        self.publicCombo.setItemText(1, QtGui.QApplication.translate("PereferencesDialog", "enabled", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("PereferencesDialog", "custom ip", None, QtGui.QApplication.UnicodeUTF8))

